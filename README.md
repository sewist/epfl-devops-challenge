EPFL DevOps Challenge
===========

## The Challenge

Your challenge, should you choose to accept it, is to set up and
maintain sustainable infrastructure for simple web
application with _Ansible_.


## Specifications

Infrastructure should be created and managed by _Ansible_. It should
contain at least 3 nodes:

- One balancer with _Haproxy_.
- One application server with simple _Django_ application.
- One backend server with _MongoDB_ or _MySQL_.


## Rules

- Ansible should build and run all infrastructure by one play.
- Ansible should install critical security patches for the OS.
- Ansible should update aplication code without any rejections for end user.


## How to use it

- Install _Virtualbox_ and _Vagrant_ on your machine.
- Ensure to have a RSA key stored under "~/.ssh/id_rsa.pub".
- Run this Vagrantfile using "vagrant up".

After running this Vagranfile you will have a Class-C private network
on the 192.168.50.0 subnet with the following FQDNs and IP address for each node:

- node-1.vagrant : 192.168.50.10
- node-2.vagrant : 192.168.50.11
- node-3.vagrant : 192.168.50.12


## Advices

Be ready to answer any kind of questions about your solution.


## Bonus

Increase failover by adding one or more application/backend nodes(create
your own updated Vagrantfile to be able build your infrastructure).
Create ansible playbook for create/restore database backups.
